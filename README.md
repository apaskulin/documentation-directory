# Documentation directory

This repository is an experimental project to index and track Wikimedia technical documentation pages.
For more information, see the [project page on mediawiki.org](https://www.mediawiki.org/wiki/Documentation/Content_types).
